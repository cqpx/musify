# Musify

> A Vue.js project

## Dev Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production
npm run build
```

## Run the server
``` bash
python -m SimpleHTTPServer 3000
```

Visit localhost:3000