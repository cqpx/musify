import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'

Vue.use(VueRouter)

const router = new VueRouter({
  // mode: 'history',
  // base: __dirname,
  routes: [
    { path: '/', component: App },
    { path: '/:id', component: App },
  ]
})

// router.beforeEach(function (transition) {
//   window.scrollTo(0, 0)
//   console.log(transition)
//   // transition.next()
// })

new Vue({
  router,
  render: function (createElement) {
    return createElement('router-view')
  }
}).$mount('#app')
